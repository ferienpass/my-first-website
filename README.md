My first Website project
========================

Available online at:
- https://ferienpass.gitlab.io/my-first-website/

How do I use Git?
-----------------

In the project directory run the following commands:

1. `git status` (nachsehen, wie der aktuelle Zustand ist)
2. `git add -v .` (alle aktuellen Änderungen zum Speichern vorbereiten)
3. `git commit -m "<Ein Kommentar, der die Änderungen beschreibt>"` (alle Änderungen lokal speichern)
4. `git push` (alle gespeicherten Änderungen auf GitLab uploaden)
